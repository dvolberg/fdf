# Fdf

This repository contains the __FDF__ project of 42

Usage:  
	
	make
	./fdf map/[map.fdf]  

Enjoy it!  

![42.png](https://raw.githubusercontent.com/dvolberg/Fdf/master/map/42.png)
![guadeloupe.png](https://raw.githubusercontent.com/dvolberg/Fdf/master/map/guadeloupe.png)